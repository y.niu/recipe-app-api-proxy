# Recipe App API Proxy 

NGINX Proxy app for recipe app API

## Usage

### Environment Variables 

* 'LISTEN_PORT' - Port to Listen on (default: '8000')
* 'APP_HOST' - Hosename of the app to foreard reqtuest to (default: 'app')
* 'APP_PORT' - Port of teh app to forward requests to (default: '9000')